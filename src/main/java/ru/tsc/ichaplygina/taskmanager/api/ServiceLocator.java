package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.List;

public interface ServiceLocator {

    List<AbstractCommand> getCommandList();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

}
