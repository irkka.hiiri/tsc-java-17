package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(final String name, final String description) throws AbstractException;

    Project completeById(final String id) throws AbstractException;

    Project completeByIndex(final int index) throws AbstractException;

    Project completeByName(final String name) throws AbstractException;

    Project findById(final String id) throws AbstractException;

    Project findByName(final String name) throws AbstractException;

    Project findByIndex(final int index) throws AbstractException;

    boolean isEmpty();

    Project startById(final String id) throws AbstractException;

    Project startByIndex(final int index) throws AbstractException;

    Project startByName(final String name) throws AbstractException;

    Project updateByIndex(final int index, final String name, final String description) throws AbstractException;

    Project updateById(final String id, final String name, final String description) throws AbstractException;

    Project updateStatus(String id, Status status) throws AbstractException;

}
