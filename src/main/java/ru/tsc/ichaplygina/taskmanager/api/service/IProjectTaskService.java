package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    void clearProjects();

    int getProjectSize();

    Task removeTaskFromProject(String projectId, String taskId) throws AbstractException;

    List<Task> findAllTasksByProjectId(String projectId, final Comparator<Task> taskComparator) throws AbstractException;

    Project removeProjectById(final String projectId) throws AbstractException;

    Project removeProjectByIndex(final int projectIndex) throws AbstractException;

    Project removeProjectByName(final String projectName) throws AbstractException;

    Task addTaskToProject(String projectId, String taskId) throws AbstractException;

}
