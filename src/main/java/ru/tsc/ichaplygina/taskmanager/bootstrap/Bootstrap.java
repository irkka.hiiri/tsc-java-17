package ru.tsc.ichaplygina.taskmanager.bootstrap;

import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.repository.*;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.command.system.*;
import ru.tsc.ichaplygina.taskmanager.command.task.*;
import ru.tsc.ichaplygina.taskmanager.command.project.*;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandDescriptionEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandNameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.*;
import ru.tsc.ichaplygina.taskmanager.repository.*;
import ru.tsc.ichaplygina.taskmanager.service.*;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    private void initData() {
        taskRepository.add(new Task("aaa", "Sample Task"));
        taskRepository.add(new Task("ggg", "Sample Task"));
        taskRepository.add(new Task("ccc", "Sample Task"));
        taskRepository.add(new Task("ttt", "Sample Task"));
        taskRepository.add(new Task("bbb", "Sample Task"));
        projectRepository.add(new Project("eee", "Sample Project"));
        projectRepository.add(new Project("yyy", "Sample Project"));
        projectRepository.add(new Project("xxx", "Sample Project"));
        projectRepository.add(new Project("333", "Sample Project"));
        projectRepository.add(new Project("hhh", "Sample Project"));
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

    public void run(final String... args) {
        initCommands();
        if (args == null || args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommand(args);
            } catch (final AbstractException e) {
                logService.error(e);
            }
    }

    public void processInput() {
        logService.info("Program run in command-line mode.");
        initData();
        showWelcome();
        String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (final AbstractException e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    public void initCommands() {
        initTaskCommands();
        initProjectCommands();
        initSystemCommands();
    }

    public void initSystemCommands() {
        registerCommand(new InfoCommand());
        registerCommand(new AboutCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new ListCommandsCommand());
        registerCommand(new ListArgumentsCommand());
        registerCommand(new ExitCommand());
    }

    public void initTaskCommands() {
        registerCommand(new TaskListCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskShowByNameCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskRemoveByNameCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskStartByNameCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCompleteByNameCommand());
        registerCommand(new TaskListByProjectCommand());
        registerCommand(new TaskAddToProjectCommand());
        registerCommand(new TaskRemoveFromProjectCommand());
    }

    public void initProjectCommands() {
        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectShowByNameCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectRemoveByNameCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectStartByNameCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCompleteByNameCommand());
    }

    public void registerCommand(final AbstractCommand command) {
        try {
            final String terminalCommand = command.getCommand();
            final String commandDescription = command.getDescription();
            final String commandArgument = command.getArgument();
            if (isEmptyString(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyString(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commands.put(terminalCommand, command);
            if (isEmptyString(commandArgument)) return;
            arguments.put(commandArgument, command);
        } catch (AbstractException e) {
            logService.error(e);
        }
    }

    public void executeCommand(final String commandName) throws AbstractException {
        if (isEmptyString(commandName)) return;
        final AbstractCommand command = commands.get(commandName);
        if (command == null) throw new CommandNotFoundException(commandName);
        command.execute();
    }

    public void executeCommand(final String[] params) throws AbstractException {
        if (params == null || params.length == 0) return;
        final AbstractCommand command = arguments.get(params[0]);
        if (command == null) throw new CommandNotFoundException(params[0]);
        command.execute();
    }

    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}
