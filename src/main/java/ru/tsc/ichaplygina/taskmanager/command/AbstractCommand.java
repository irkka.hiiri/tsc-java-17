package ru.tsc.ichaplygina.taskmanager.command;

import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute() throws AbstractException;

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

}
