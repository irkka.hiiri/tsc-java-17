package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECTS_CLEAR;
    }

    @Override
    public String getDescription() {
        return "delete all projects";
    }

    @Override
    public void execute() {
        final int count = getProjectTaskService().getProjectSize();
        getProjectTaskService().clearProjects();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

}
