package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_COMPLETE_BY_ID;
    }

    @Override
    public String getDescription() {
        return "complete project by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        getProjectService().completeById(id);
        showUpdateResult();
    }

}
