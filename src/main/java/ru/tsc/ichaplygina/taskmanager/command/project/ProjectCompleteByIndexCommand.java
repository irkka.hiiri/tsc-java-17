package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_COMPLETE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "complete project by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        getProjectService().completeByIndex(index - 1);
        showUpdateResult();
    }

}
