package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_COMPLETE_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "complete project by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        getProjectService().completeByName(name);
        showUpdateResult();
    }

}
