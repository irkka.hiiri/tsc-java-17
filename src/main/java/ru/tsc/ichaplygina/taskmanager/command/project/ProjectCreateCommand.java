package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_CREATE;
    }

    @Override
    public String getDescription() {
        return "create a new project";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().add(name, description);
        printLinesWithEmptyLine(PROJECT_CREATED);
    }

}
