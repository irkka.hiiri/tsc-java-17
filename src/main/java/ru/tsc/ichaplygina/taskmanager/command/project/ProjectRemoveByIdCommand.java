package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String getDescription() {
        return "remove project by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (getProjectTaskService().removeProjectById(id) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

}
