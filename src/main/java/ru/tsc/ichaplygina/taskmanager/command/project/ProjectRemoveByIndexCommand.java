package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "remove project by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (getProjectTaskService().removeProjectByIndex(index - 1) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

}
