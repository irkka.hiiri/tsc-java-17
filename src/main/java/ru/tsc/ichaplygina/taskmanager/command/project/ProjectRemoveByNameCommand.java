package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_REMOVE_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "remove project by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        if (getProjectTaskService().removeProjectByName(name) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

}
