package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_SHOW_BY_ID;
    }

    @Override
    public String getDescription() {
        return "show project by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        final Project project = getProjectService().findById(id);
        showProject(project);
    }

}
