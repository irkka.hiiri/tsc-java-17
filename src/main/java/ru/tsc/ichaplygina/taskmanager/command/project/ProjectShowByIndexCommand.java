package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_SHOW_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "show project by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        final Project project = getProjectService().findByIndex(index - 1);
        showProject(project);
    }

}
