package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_START_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "start project by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        getProjectService().startByIndex(index - 1);
        showUpdateResult();
    }

}
