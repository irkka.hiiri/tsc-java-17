package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_START_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "start project by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        getProjectService().startByName(name);
        showUpdateResult();
    }

}
