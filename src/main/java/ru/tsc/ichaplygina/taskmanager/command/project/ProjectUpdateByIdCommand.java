package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (getProjectService().findById(id) == null) throw new ProjectNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().updateById(id, name, description);
        showUpdateResult();
    }

}
