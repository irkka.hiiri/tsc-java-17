package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "update project by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (getProjectService().findByIndex(index - 1) == null) throw new ProjectNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
