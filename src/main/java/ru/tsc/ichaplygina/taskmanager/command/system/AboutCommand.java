package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_ABOUT;
    }

    @Override
    public String getDescription() {
        return "show developer info";
    }

    @Override
    public String getArgument() {
        return ARG_ABOUT;
    }

    @Override
    public void execute() {
        printLinesWithEmptyLine(APP_ABOUT);
    }

}
