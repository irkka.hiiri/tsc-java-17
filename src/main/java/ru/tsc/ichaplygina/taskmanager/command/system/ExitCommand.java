package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.CMD_EXIT;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_EXIT;
    }

    @Override
    public String getDescription() {
        return "quit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
