package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class HelpCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_HELP;
    }

    @Override
    public String getDescription() {
        return "show this message";
    }

    @Override
    public String getArgument() {
        return ARG_HELP;
    }

    @Override
    public void execute() {
        System.out.println(APP_HELP_HINT_TEXT);
        printList(serviceLocator.getCommandList());
    }

}
