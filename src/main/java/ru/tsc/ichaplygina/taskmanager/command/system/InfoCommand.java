package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class InfoCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_INFO;
    }

    @Override
    public String getDescription() {
        return "show system info";
    }

    @Override
    public String getArgument() {
        return ARG_INFO;
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        final int numberOfCpus = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryDisplayed;
        if (maxMemory == Long.MAX_VALUE) maxMemoryDisplayed = SYSINFO_NO_LIMIT_TEXT;
        else maxMemoryDisplayed = NumberUtil.convertBytesToString(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        printLinesWithEmptyLine(SYSINFO_PROCESSORS + numberOfCpus,
                SYSINFO_FREE_MEMORY + NumberUtil.convertBytesToString(freeMemory),
                SYSINFO_MAX_MEMORY + maxMemoryDisplayed,
                SYSINFO_TOTAL_MEMORY + NumberUtil.convertBytesToString(totalMemory),
                SYSINFO_USED_MEMORY + NumberUtil.convertBytesToString(usedMemory));
    }

}
