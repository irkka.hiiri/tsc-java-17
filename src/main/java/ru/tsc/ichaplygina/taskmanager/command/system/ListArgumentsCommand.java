package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;

public class ListArgumentsCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_LIST_COMMANDS;
    }

    @Override
    public String getDescription() {
        return "list available commands";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println();
        for (AbstractCommand command : serviceLocator.getCommandList()) {
            System.out.println(command.getCommand());
        }
        System.out.println();
    }

}
