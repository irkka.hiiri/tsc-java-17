package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class ListCommandsCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_LIST_ARGUMENTS;
    }

    @Override
    public String getDescription() {
        return "list available command-line arguments";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println();
        for (AbstractCommand command : serviceLocator.getCommandList()) {
            if (isEmptyString(command.getArgument())) continue;
            System.out.println(command.getArgument());
        }
        System.out.println();
    }

}
