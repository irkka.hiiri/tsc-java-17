package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class VersionCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return CMD_VERSION;
    }

    @Override
    public String getDescription() {
        return "show version info";
    }

    @Override
    public String getArgument() {
        return ARG_VERSION;
    }

    @Override
    public void execute() {
        printLinesWithEmptyLine(APP_VERSION);
    }

}
