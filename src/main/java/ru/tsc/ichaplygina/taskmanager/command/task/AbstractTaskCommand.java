package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public String getArgument() {
        return null;
    }

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showTask(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        printLinesWithEmptyLine(task);
    }

    protected void showRemoveResult() {
        printLinesWithEmptyLine(TASK_REMOVED);
    }

    protected void showUpdateResult() {
        printLinesWithEmptyLine(TASK_UPDATED);
    }

    protected void showRemoveTaskFromProjectResult() {
        printLinesWithEmptyLine(TASK_REMOVED_FROM_PROJECT);
    }

    protected void showAddTaskToProjectResult() {
        printLinesWithEmptyLine(TASK_ADDED_TO_PROJECT);
    }

}
