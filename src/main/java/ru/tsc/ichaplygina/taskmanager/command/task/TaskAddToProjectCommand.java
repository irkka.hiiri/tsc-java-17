package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskAddToProjectCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_ADD_TO_PROJECT;
    }

    @Override
    public String getDescription() {
        return "add task to a project";
    }

    @Override
    public void execute() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        getProjectTaskService().addTaskToProject(projectId, taskId);
        showAddTaskToProjectResult();
    }

}
