package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASKS_CLEAR;
    }

    @Override
    public String getDescription() {
        return "delete all tasks";
    }

    @Override
    public void execute() {
        final int count = getTaskService().getSize();
        getTaskService().clear();
        printLinesWithEmptyLine(count + TASKS_CLEARED);
    }

}
