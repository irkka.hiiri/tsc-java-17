package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_COMPLETE_BY_ID;
    }

    @Override
    public String getDescription() {
        return "complete task by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        getTaskService().completeById(id);
        showUpdateResult();
    }

}
