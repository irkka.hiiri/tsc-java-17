package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_COMPLETE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "complete task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        getTaskService().completeByIndex(index - 1);
        showUpdateResult();
    }

}
