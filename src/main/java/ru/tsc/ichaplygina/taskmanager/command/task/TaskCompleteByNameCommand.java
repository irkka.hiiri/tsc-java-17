package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskCompleteByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_COMPLETE_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "complete task by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        getTaskService().completeByName(name);
        showUpdateResult();
    }

}
