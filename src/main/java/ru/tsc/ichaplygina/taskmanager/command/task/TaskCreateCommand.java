package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_CREATE;
    }

    @Override
    public String getDescription() {
        return "create a new task";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().add(name, description);
        printLinesWithEmptyLine(TASK_CREATED);
    }

}
