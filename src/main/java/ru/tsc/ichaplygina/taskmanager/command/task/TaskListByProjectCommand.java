package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListByProjectCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASKS_LIST_BY_PROJECT;
    }

    @Override
    public String getDescription() {
        return "show all tasks in a project";
    }

    @Override
    public void execute() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final Comparator<Task> taskComparator = readComparator();
        final List<Task> taskList = getProjectTaskService().findAllTasksByProjectId(projectId, taskComparator);
        if (taskList == null) throw new ProjectNotFoundException();
        if (taskList.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND_IN_PROJECT);
            return;
        }
        printListWithIndexes(taskList);
    }

}
