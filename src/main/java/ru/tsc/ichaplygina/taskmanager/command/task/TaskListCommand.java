package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASKS_LIST;
    }

    @Override
    public String getDescription() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        if (getTaskService().isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND);
            return;
        }
        Comparator<Task> comparator = readComparator();
        printListWithIndexes(getTaskService().findAll(comparator));
    }

}
