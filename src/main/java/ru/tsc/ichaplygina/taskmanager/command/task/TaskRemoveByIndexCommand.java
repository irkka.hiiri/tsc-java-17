package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_REMOVE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "remove task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (getTaskService().removeByIndex(index - 1) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

}
