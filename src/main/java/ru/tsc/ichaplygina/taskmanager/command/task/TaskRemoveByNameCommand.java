package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_REMOVE_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "remove task by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        if (getTaskService().removeByName(name) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

}
