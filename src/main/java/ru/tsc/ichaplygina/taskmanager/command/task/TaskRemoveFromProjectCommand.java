package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskRemoveFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_REMOVE_FROM_PROJECT;
    }

    @Override
    public String getDescription() {
        return "remove task from a project";
    }

    @Override
    public void execute() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        getProjectTaskService().removeTaskFromProject(projectId, taskId);
        showRemoveTaskFromProjectResult();
    }

}
