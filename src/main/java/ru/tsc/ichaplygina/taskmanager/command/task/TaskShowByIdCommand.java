package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_SHOW_BY_ID;
    }

    @Override
    public String getDescription() {
        return "show task by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        final Task task = getTaskService().findById(id);
        showTask(task);
    }

}
