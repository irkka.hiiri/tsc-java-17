package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_SHOW_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "show task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        final Task task = getTaskService().findByIndex(index - 1);
        showTask(task);
    }

}
