package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_SHOW_BY_NAME;
    }

    @Override
    public String getDescription() {
        return "show task by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        final Task task = getTaskService().findByName(name);
        showTask(task);
    }

}
