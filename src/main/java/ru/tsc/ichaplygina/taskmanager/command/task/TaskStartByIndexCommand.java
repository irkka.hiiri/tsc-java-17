package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_START_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "start task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        getTaskService().startByIndex(index - 1);
        showUpdateResult();
    }

}
