package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_UPDATE_BY_ID;
    }

    @Override
    public String getDescription() {
        return "update task by id";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (getTaskService().findById(id) == null) throw new TaskNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().updateById(id, name, description);
        showUpdateResult();
    }

}
