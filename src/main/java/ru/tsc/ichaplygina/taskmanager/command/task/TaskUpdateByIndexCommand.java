package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String getDescription() {
        return "update task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (getTaskService().findByIndex(index - 1) == null) throw new TaskNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
