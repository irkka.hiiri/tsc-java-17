package ru.tsc.ichaplygina.taskmanager.exception;

public abstract class AbstractException extends Exception {

    public AbstractException(String message) {
        super(message);
    }

}
