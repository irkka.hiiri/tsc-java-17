package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class CommandDescriptionEmptyException extends AbstractException {

    private static final String CMD_DESCRIPTION_EMPTY = "Error! Command description cannot be empty.";

    public CommandDescriptionEmptyException() {
        super(CMD_DESCRIPTION_EMPTY);
    }

}
