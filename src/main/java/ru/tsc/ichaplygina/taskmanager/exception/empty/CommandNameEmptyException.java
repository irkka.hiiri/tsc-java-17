package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class CommandNameEmptyException extends AbstractException {

    private static final String CMD_NAME_EMPTY = "Error! Command name cannot be empty.";

    public CommandNameEmptyException() {
        super(CMD_NAME_EMPTY);
    }
}
