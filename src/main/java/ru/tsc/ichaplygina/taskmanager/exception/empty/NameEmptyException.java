package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class NameEmptyException extends AbstractException {

    private static final String NAME_EMPTY = "Error! Name is empty.";

    public NameEmptyException() {
        super(NAME_EMPTY);
    }

}
