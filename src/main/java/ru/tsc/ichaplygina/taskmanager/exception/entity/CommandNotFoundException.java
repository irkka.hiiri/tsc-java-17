package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class CommandNotFoundException extends AbstractException {

    private static final String APP_UNKNOWN_CMD = "Unknown command or argument: ";

    public CommandNotFoundException(final String command) {
        super(APP_UNKNOWN_CMD + command);
    }

}
