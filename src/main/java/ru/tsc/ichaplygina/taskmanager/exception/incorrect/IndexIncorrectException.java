package ru.tsc.ichaplygina.taskmanager.exception.incorrect;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    private static final String INCORRECT_INDEX = "Error! Incorrect index: ";

    public IndexIncorrectException(final int index) {
        super(INCORRECT_INDEX + index);
    }

}
