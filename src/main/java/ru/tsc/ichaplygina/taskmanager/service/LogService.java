package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

public class LogService implements ILogService {

    private static final String COMMANDS = "COMMANDS";

    private static final String ERRORS = "ERRORS";

    private static final String MESSAGES = "MESSAGES";

    private static final String COMMAND_FILE = "access.log";

    private static final String ERROR_FILE = "errors.log";

    private static final String MESSAGES_FILE = "messages.log";

    private static final Logger commandLogger = Logger.getLogger(COMMANDS);

    private static final Logger errorLogger = Logger.getLogger(ERRORS);

    private static final Logger messageLogger = Logger.getLogger(MESSAGES);

    private static final Logger rootLogger = Logger.getGlobal();

    private static final String LOGGER_PROPERTIES_FILE = "logging.properties";

    private final LogManager logManager = LogManager.getLogManager();

    private Boolean consoleEnabled = true;

    {
        initProperties();
        registerHandlers(commandLogger, COMMAND_FILE);
        registerHandlers(errorLogger, ERROR_FILE);
        registerHandlers(messageLogger, MESSAGES_FILE);
        if (consoleEnabled) {
            commandLogger.addHandler(new ConsoleHandler());
            errorLogger.addHandler(new ConsoleHandler());
        }
    }

    public LogService() {
    }

    public LogService(final Boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    public Boolean getConsoleEnabled() {
        return consoleEnabled;
    }

    public void setConsoleEnabled(Boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    public void initProperties() {
        try {
            FileInputStream configFile = new FileInputStream(LOGGER_PROPERTIES_FILE);
            logManager.readConfiguration(configFile);
        } catch (IOException e) {
            rootLogger.log(SEVERE, "Error! Failed to initialize logging properties: " + e.getMessage(), e);
        }
    }

    public void registerHandlers(final Logger logger, final String fileName) {
        try {
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            rootLogger.severe("Error! Failed to register logging file handlers: " + e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message.isEmpty()) return;
        messageLogger.info(message);
    }

    @Override
    public void command(final String message) {
        if (message.isEmpty()) return;
        commandLogger.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errorLogger.log(WARNING, e.getMessage(), e);
    }

}
