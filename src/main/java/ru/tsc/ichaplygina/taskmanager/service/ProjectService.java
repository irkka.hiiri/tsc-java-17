package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public boolean isEmpty() {
        return projectRepository.isEmpty();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return projectRepository.findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Project project = new Project(name, description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (!projectRepository.isFoundById(id)) throw new ProjectNotFoundException();
        return projectRepository.update(id, name, description);
    }

    @Override
    public Project updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectRepository.getId(index);
        return updateById(id, name, description);
    }

    @Override
    public Project updateStatus(String id, Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (!projectRepository.isFoundById(id)) throw new ProjectNotFoundException();
        return projectRepository.updateStatus(id, status);
    }

    @Override
    public Project startById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Project startByIndex(int index) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectRepository.getId(index);
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Project startByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = projectRepository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Project completeById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Project completeByIndex(int index) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectRepository.getId(index);
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Project completeByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = projectRepository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        return updateStatus(id, Status.COMPLETED);
    }

}
