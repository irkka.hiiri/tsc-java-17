package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.EMPTY;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public int getProjectSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task addTaskToProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (!taskRepository.isFoundById(taskId)) throw new TaskNotFoundException();
        if (!projectRepository.isFoundById(projectId)) throw new ProjectNotFoundException();
        return taskRepository.updateProjectId(taskId, projectId);
    }

    @Override
    public Task removeTaskFromProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (!taskRepository.isFoundById(taskId)) throw new TaskNotFoundException();
        if (!projectRepository.isFoundById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.isTaskInProject(taskId, projectId)) throw new TaskNotFoundException();
        return taskRepository.updateProjectId(taskId, EMPTY);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final Comparator<Task> taskComparator)
            throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (taskComparator == null) return taskRepository.findAllByProjectId(projectId);
        return taskRepository.findAllByProjectId(projectId, taskComparator);
    }

    @Override
    public Project removeProjectById(final String projectId) throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeProjectByIndex(final int projectIndex) throws AbstractException {
        if (isInvalidListIndex(projectIndex, projectRepository.getSize()))
            throw new IndexIncorrectException(projectIndex + 1);
        final String projectId = projectRepository.getId(projectIndex);
        return removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByName(final String projectName) throws AbstractException {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        final String projectId = projectRepository.getId(projectName);
        if (projectId == null) throw new ProjectNotFoundException();
        return removeProjectById(projectId);
    }

    @Override
    public void clearProjects() {
        for (final Project project : projectRepository.findAll())
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clear();
    }

}
